
provider "aws" {
    region = "eu-west-3"
}

resource "aws_security_group" "my_sg" {
    name        = "my-sec-group"
    description = "allow-ssh and http"
    vpc_id      = var.vpc_id
    ingress {
        protocol = "TCP"
        from_port = 22
        to_port = 22
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "TCP"
        from_port = "80"
        to_port = "80"
        cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
        protocol = "-1"
        from_port = 0 
        to_port = 0
        cidr_blocks = ["0.0.0.0/0"]
   }
}


resource "aws_instance" "my_instance" {
    ami = var.image_id
    instance_type = var.machine_type
    key_name = "harsh-paris"
    vpc_security_group_ids = ["sg-0fa9f5830db289b96", aws_security_group.my_sg.id]
    tags = { 
        Name = "my_instance"
        env = "dev"
    }
}

resource "aws_instance" "another_instance" {
    ami = var.image_id
    instance_type = var.machine_type
    key_name = "harsh-paris"
    vpc_security_group_ids = ["sg-0fa9f5830db289b96"]
    tags = {
        Name = "another_instance"
        env = "dev"
    }
}

variable "image_id" {
    default = "ami-01d21b7be69801c2f"
}

variable "machine_type" {
    default = "t2.micro"
}   

variable "vpc_id" {
    default = "vpc-0c8b62cf80b02805d"
}