#  Terraform 

## What is Terraform ?
 IAC 

 ## Why Terraform ? 

 ## Terraform Language

 .tf / .tf.json
 HCL / JSON 

 ## Terraform Blocks 

 - Provider
 - terraform 
 - resource
 - data 
 - output
 - variable 
 - module 

 ## Terraform Lifecycle

 - init 
 - plan 
 - apply 
 - destroy

 ## Dependencies

 Implicit
 Explicit

 